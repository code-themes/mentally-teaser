      <footer class="container my-14 max-w-4xl" role="contentinfo" itemscope itemtype="https://schema.org/WPFooter">
        <div
          class="flex justify-between flex-col md:flex-row text-sm text-center md:text-left"
        >
          <nav class="md:order-2" role="navigation">
            <?php wp_nav_menu( array(
                'container' => false, // remove nav container
                'menu' => __( 'Footer Links', 'platetheme' ), // nav name
                'menu_class' => 'footer-links', // adding custom nav class
                'theme_location' => 'footer-links', // where it's located in the theme
                'depth' => 0, // limit the depth of the nav
            )); ?>
          </nav>
          <div class="mt-1 md:mt-0 md:order-1">
            &copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>
            <?php $text = get_field('footer_copyright_text', 'option');
            if (!empty($text)): ?>
              &ndash; <?php echo $text; ?>
            <?php endif; ?>
          </div>
        </div>
      </footer>

		</div>

		<?php // all js scripts are loaded in library/functions.php ?>
		<?php wp_footer(); ?>

	</body>
</html>
