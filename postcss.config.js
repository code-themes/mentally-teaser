module.exports = {
  plugins: [
    require("tailwindcss"),
    require("autoprefixer"),
    require("postcss-nested"),
    require("postcss-font-magician")({
      formats: "woff2 woff ttf eot otf",
      custom: {
        BwModelica: {
          variants: {
            normal: {
              100: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-Hairline.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-Hairline.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-Hairline.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-Hairline.otf",
                },
              },
              200: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-Thin.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-Thin.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-Thin.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-Thin.otf",
                },
              },
              300: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-Light.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-Light.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-Light.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-Light.otf",
                },
              },
              400: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-Regular.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-Regular.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-Regular.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-Regular.otf",
                },
              },
              500: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-Medium.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-Medium.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-Medium.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-Medium.otf",
                },
              },
              700: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-Bold.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-Bold.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-Bold.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-Bold.otf",
                },
              },
              800: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-ExtraBold.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-ExtraBold.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-ExtraBold.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-ExtraBold.otf",
                },
              },
              900: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-Black.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-Black.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-Black.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-Black.otf",
                },
              },
            },
            italic: {
              100: {
                url: {
                  woff2:
                    "fonts/Bw Modelica/woff2/BwModelica-HairlineItalic.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-HairlineItalic.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-HairlineItalic.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-HairlineItalic.otf",
                },
              },
              200: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-ThinItalic.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-ThinItalic.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-ThinItalic.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-ThinItalic.otf",
                },
              },
              300: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-LightItalic.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-LightItalic.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-LightItalic.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-LightItalic.otf",
                },
              },
              400: {
                url: {
                  woff2:
                    "fonts/Bw Modelica/woff2/BwModelica-RegularItalic.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-RegularItalic.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-RegularItalic.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-RegularItalic.otf",
                },
              },
              500: {
                url: {
                  woff2:
                    "fonts/Bw Modelica/woff2/BwModelica-MediumItalic.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-MediumItalic.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-MediumItalic.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-MediumItalic.otf",
                },
              },
              700: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-BoldItalic.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-BoldItalic.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-BoldItalic.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-BoldItalic.otf",
                },
              },
              800: {
                url: {
                  woff2:
                    "fonts/Bw Modelica/woff2/BwModelica-ExtraBoldItalic.woff2",
                  woff:
                    "fonts/Bw Modelica/woff/BwModelica-ExtraBoldItalic.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-ExtraBoldItalic.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-ExtraBoldItalic.otf",
                },
              },
              900: {
                url: {
                  woff2: "fonts/Bw Modelica/woff2/BwModelica-BlackItalic.woff2",
                  woff: "fonts/Bw Modelica/woff/BwModelica-BlackItalic.woff",
                  eot: "fonts/Bw Modelica/eot/BwModelica-BlackItalic.eot",
                  otf: "fonts/Bw Modelica/otf/BwModelica-BlackItalic.otf",
                },
              },
            },
          },
        },
      },
    }),
  ],
};
