const defaultTheme = require("tailwindcss/defaultTheme");
const hexRgb = require("hex-rgb");

function rgba(hex, alpha) {
  const { red, green, blue } = hexRgb(hex);
  return `rgba(${red}, ${green}, ${blue}, ${alpha})`;
}

// https://javisperez.github.io/tailwindcolorshades/#/?primary=1385C3&secondary=77C12B&tv=1
const colors = {
  primary: {
    100: "#E7F3F9",
    200: "#C4E1F0",
    300: "#A1CEE7",
    400: "#5AAAD5",
    500: "#1385C3",
    600: "#1178B0",
    700: "#0B5075",
    800: "#093C58",
    900: "#06283B",
  },
  secondary: {
    100: "#F1F9EA",
    200: "#DDF0CA",
    300: "#C9E6AA",
    400: "#A0D46B",
    500: "#77C12B",
    600: "#6BAE27",
    700: "#47741A",
    800: "#365713",
    900: "#243A0D",
  },
  cream: "#F0F0F0",
  silver: "#E5E5E5",
  light: "#DBDBDB",
  dark: "#1F3B51",
};

module.exports = {
  theme: {
    extend: {
      borderRadius: {
        xl: "24px",
      },
      boxShadow: {
        "solid-xl": `0 0 0 12px currentColor`,
        "solid-primary": `0 0 0 1px ${colors.primary[500]}`,
        "outline-white": `0 0 0 3px ${rgba(defaultTheme.colors.white, 0.45)}`,
        "outline-primary": `0 0 0 3px ${rgba(colors.primary[500], 0.45)}`,
      },
      colors,
      container: {
        center: true,
        padding: "1.5rem",
      },
      fontFamily: {
        sans: ["BwModelica", ...defaultTheme.fontFamily.sans],
      },
      gradients: {
        gradient: ["0deg", colors.secondary[500], colors.primary[500]],
      },
      letterSpacing: {
        "extra-wide": ".35em",
      },
    },
  },
  variants: {
    scale: ["responsive", "hover", "focus", "group-hover", "group-focus"],
    translate: ["responsive", "hover", "focus", "group-hover", "group-focus"],
  },
  plugins: [
    require("@tailwindcss/ui"),
    require("tailwindcss-hyphens"),
    require("tailwindcss-plugins/gradients"),
  ],
};
