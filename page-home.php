<?php
/*
 Template Name: Home page
*/
?>

<?php get_header(); ?>

  <div class="md:px-12 pt-12 pb-20 bg-gradient">
    <div class="-mt-12 px-6 md:px-20 py-5 text-right">
      <?php wp_nav_menu(array(
          'container' => false, // remove nav container
          'menu' => __('Main Menu', 'platetheme'), // nav name
          'menu_class' => 'top-bar', // adding custom nav class
          'theme_location' => 'main-nav', // where it's located in the theme
          'depth' => 0, // limit the depth of the nav
      )); ?>
    </div>
    <div class="bg-white md:rounded-xl md:px-20 pt-40 pb-56">
      <div class="container max-w-4xl">

        <?php $text = get_field('teaser_text');
        if (!empty($text)): ?>
          <div class="md:pl-32">
            <div
              class="uppercase md:text-lg font-bold tracking-extra-wide leading-none"
            >
              <?php echo $text; ?>
            </div>
          </div>
        <?php endif; ?>

        <div class="mt-5 md:mt-0">
          <?php echo load_svg('src/images/logo.svg', 'h-28 w-auto max-w-full'); ?>
        </div>

        <div class="mt-7">
            
          <main id="main" class="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="https://schema.org/Blog">

            <?php // Edit the loop in /templates/block-loop. Or roll your own. ?>
            <?php get_template_part( 'templates/full-loop'); ?>

          </main>

        </div>

        <div class="mt-20">
          <?php get_template_part('templates/opt-in'); ?>
        </div>
      </div>
    </div>
  </div>

  <div class="-mt-56">
    <?php get_template_part('templates/slider'); ?>
  </div>

  <?php
  $text = get_field('contact_text');
  $email_address = get_field('contact_email_address');
  $email_subject = get_field('contact_email_subject');
  ?>
  <?php if (!empty($text) || !empty($email_address)): ?>
    <div class="container my-40 max-w-4xl">
      <div class="contact">
        <?php echo $text; ?>

        <?php if (!empty($email_address)): ?>
          <div class="-mt-1 text-3xl">
            <a href="mailto:<?php echo $email_address; ?><?php echo !empty($email_subject) ? '?subject='.rawurlencode($email_subject) : ''; ?>"><?php echo $email_address; ?></a
            >
          </div>
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>

<?php get_footer(); ?>